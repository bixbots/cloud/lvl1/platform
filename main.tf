locals {
  region      = "us-east-1"
  application = "platform"
  environment = "lvl1"

  root_zone = "${local.environment}.cloud"

  common_tags = {
    "TerraformModule" = "${local.environment}/${local.application}"
  }
}

module "tags_base" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/tags-base.git?ref=v1"

  application = local.application
  environment = local.environment
  lifespan    = "permanent"

  tags = local.common_tags
}

module "ssh_keypair" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair.git?ref=v1"

  application = local.application
  environment = local.environment

  public_key = file("${path.module}/platform-lvl1.pubkey.pem")
}

resource "aws_route53_zone" "root" {
  name    = local.root_zone
  comment = "Root DNS zone"

  force_destroy = true

  tags = merge(module.tags_base.tags, {
    "Name"    = local.root_zone
    "Role"    = "DNS"
    "VPCName" = local.environment
  })
}

module "vpc" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/vpc-nat-asg.git?ref=v2"

  region  = local.region
  name    = local.environment
  zone_id = aws_route53_zone.root.zone_id

  cidr            = "10.131.0.0/16"
  public_subnets  = ["10.131.0.0/21", "10.131.64.0/21", "10.131.128.0/21"]
  private_subnets = ["10.131.16.0/20", "10.131.80.0/20", "10.131.144.0/20"]
  data_subnets    = ["10.131.8.0/21", "10.131.72.0/21", "10.131.136.0/21"]
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]

  ami_id              = "ami-02f55863fb09f62b1"
  instance_type       = "t3.nano"
  spot_price          = "0.0052"
  key_name            = module.ssh_keypair.key_name
  enable_bastion_role = true

  tags = module.tags_base.tags
}

module "acme-lambda-config" {
  source = "git::https://gitlab.com/bixbots/cloud/terraform-modules/acme-lambda-config.git?ref=v1"

  config_name    = "${local.environment}.yml"
  config_content = file("${path.module}/${local.environment}.yml")
}
