# lvl1/platform

## Summary

Deploys platform specific infrastructure for the `lvl1` environment.

* SSH key pairs (`platform-lvl1`)
* Root Route53 zone (`lvl1.cloud`)
* VPC (`lvl1`)
* ACME Lambda configuration for provisioning wilcard certificate (`*.lvl1.cloud`)

## Resources

In addition to these dependent modules:

* [ssh-keypair](https://gitlab.com/bixbots/cloud/terraform-modules/ssh-keypair)
* [vpc-nat-asg](https://gitlab.com/bixbots/cloud/terraform-modules/vpc-nat-asg)
* [acme-lambda-config](https://gitlab.com/bixbots/cloud/terraform-modules/acme-lambda-config)

This module creates the following resources:

* [aws_route53_zone](https://www.terraform.io/docs/providers/aws/r/route53_zone.html)
